package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"regexp"
	"strings"
	"syscall"
	"time"
)

func getenv(key, defaultValue string) string {
	if v := os.Getenv(key); v != "" {
		return v
	}
	return defaultValue
}

var (
	addr   = flag.String("addr", getenv("ADDR", ":3000"), "TCP `addr` to listen on")
	imgDir = flag.String("images", getenv("ROOT_DIR", "imgs"), "`path` of the image content")

	imgRx = regexp.MustCompile(`^([^/]+)/([^/]+\.(gif|png|jpe?g))$`)
)

// Scan the image directory, expecting a two-level structure CLASS/IMAGE, where
// CLASS is an arbitrary identifier for a set of similar images.
func scanImages(dir string) (map[string][]string, error) {
	imgMap := make(map[string][]string)
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil || info.IsDir() || !info.Mode().IsRegular() {
			return err
		}

		relPath := strings.TrimPrefix(path[len(dir):], "/")
		matches := imgRx.FindStringSubmatch(relPath)
		if len(matches) > 0 {
			imgMap[matches[1]] = append(imgMap[matches[1]], matches[2])
		}
		return nil
	})
	return imgMap, err
}

func cacheHeaders(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Cache-Control", "public, max-age=31536000")
		h.ServeHTTP(w, req)
	})
}

func buildImgServer(dir string, imgMap map[string][]string) http.Handler {
	mux := http.NewServeMux()

	// Serve image content below /files/.
	mux.Handle("/files/", cacheHeaders(http.StripPrefix("/files/", http.FileServer(http.Dir(dir)))))

	// Install the hrandom selector handler for every known image class.
	for class, items := range imgMap {
		// Avoid loop aliasing in the handler.
		class := class
		items := items
		if len(items) == 0 {
			continue
		}
		log.Printf("loaded %d images in class '%s'", len(items), class)
		mux.HandleFunc("/"+class, func(w http.ResponseWriter, req *http.Request) {
			imgURL := fmt.Sprintf("/files/%s/%s", class, items[rand.Intn(len(items))])
			http.Redirect(w, req, imgURL, http.StatusFound)
		})
	}

	return mux
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	// Sanity check: the root directory must exist.
	if _, err := os.Stat(*imgDir); os.IsNotExist(err) {
		log.Fatalf("image directory %s does not exist", *imgDir)
	}

	// Scan the images.
	imgMap, err := scanImages(*imgDir)
	if err != nil {
		log.Fatal(err)
	}
	h := buildImgServer(*imgDir, imgMap)
	http.Handle("/", h)

	// Start the HTTP server, and set it up to terminate
	// gracefully on SIGINT/SIGTERM.
	srv := &http.Server{
		Addr:         *addr,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout:  600 * time.Second,
	}

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		// Gracefully terminate for 3 seconds max, then shut
		// down remaining clients.
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		err := srv.Shutdown(ctx)
		if err == context.Canceled {
			err = srv.Close()
		}
		if err != nil {
			log.Printf("error terminating server: %v", err)
		}
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	log.Printf("starting static server on %s", *addr)
	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("error: %v", err)
	}
}
