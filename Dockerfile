FROM docker.io/library/golang:1.20.1 AS build

ADD imgserver.go /src/imgserver.go
WORKDIR /src
RUN go build -tags netgo -o /imgserver imgserver.go && strip /imgserver

FROM scratch

COPY --from=build /imgserver /imgserver
COPY imgs /imgs

ENTRYPOINT ["/imgserver"]
